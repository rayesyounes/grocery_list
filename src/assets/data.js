export default [
    {
        id: 1,
        name: "Product 1",
        description: "lorem ipsum dolor sit amet",
        quantity: 5,
        price: 100,
    },
    {
        id: 2,
        name: "Product 2",
        description: "lorem ipsum dolor sit amet",
        quantity: 1,
        price: 50,
    },
    {
        id: 3,
        name: "Product 3",
        description: "lorem ipsum dolor sit amet",
        quantity: 2,
        price: 170,
    },
    {
        id: 4,
        name: "Product 4",
        description: "lorem ipsum dolor sit amet",
        quantity: 4,
        price: 43,
    },
]