import "./styles/App.css";
import "./styles/style.css";
import { useState } from "react";

import data from "./assets/data";
import AddForm from "./components/AddForm";
import SearchForm from "./components/SearchForm";
import ListeProducts from "./components/ListeProducts";

function App() {
    const [products, setProducts] = useState(data);
    const [availableProducts, setAvailableProducts] = useState(data);
    const [searchValue, setSearchValue] = useState("");

    function handelSearchChange(event) {
        setSearchValue(event.target.value);
        console.log(searchValue);
    }

    function search(event) {
        event.preventDefault();
        setProducts(availableProducts);
        setProducts((prevProducts) => {
            const searched = prevProducts.filter((product) => product.name.includes(searchValue));
            return searched;
        });
    }

    function deleteProduct(id) {
    setProducts((previousProducts) => {
        const updatedProducts = previousProducts.filter((product) => product.id !== id);
        setAvailableProducts(previousProducts => previousProducts.filter((product) => product.id !== id));
        return updatedProducts;
    });
    }

    const [formData, setFormData] = useState({
        name: "",
        description: "",
        quantity: "",
        price: "",
    });

    function handelChange(event) {
        setFormData((previousFormData) => {
            return {
                ...previousFormData,
                [event.target.name]: event.target.value,
            };
        });
    }

    function addProduct(event) {
        event.preventDefault();
        let formData_Withid;
        
        if (Object.values(formData).some((value) => value === "" || value === null)){
            console.log("Please fill in all fields before adding a product.");
            return;
        }
        
        setProducts((previeusProducts) => {
            const newId = previeusProducts.length === 0 ? 1 : previeusProducts[previeusProducts.length - 1].id + 1;
            formData_Withid = { ...formData, id: newId };
            return [...previeusProducts, formData_Withid];
        });
        
        setAvailableProducts(previeusProducts => [...previeusProducts, formData_Withid]);
    }

    return (
        <>
            <header></header>
            <main>
                <section className="form--section">
                    <SearchForm
                        handelSearchChange={handelSearchChange}
                        search={search}
                    />
                    <hr />
                    <AddForm
                        addProduct={addProduct}
                        handelChange={handelChange}
                        formData={formData}
                    />
                </section>
                <hr />
                <section className="products--section">
                    <ListeProducts
                        products={products}
                        deleteProduct={deleteProduct}
                    />
                </section>
            </main>
            <footer></footer>
        </>
    );
}

export default App;
