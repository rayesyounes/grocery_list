import PropTypes from "prop-types";
import searchLogo from "../images/Search.svg";

export default function SearchForm(props) {

    return (
        <form className="search--form">
            <input
                type="text"
                className="search--form--input"
                placeholder="Search"
                onChange={props.handelSearchChange}
            />
            <button className="search--form--button" onClick={props.search}>
                <img src={searchLogo} alt="search" />
                <span>Search</span>
            </button>
        </form>
    );
}

SearchForm.propTypes = {
    handelSearchChange: PropTypes.func.isRequired,
    search: PropTypes.func.isRequired,
};