import plusLogo from "../images/Plus Math.svg";
import PropTypes from "prop-types";

export default function AddForm(props) {
    return (
        <form className="add--form">
            <input
                type="text"
                name="name"
                className="add--form--input"
                placeholder="Product name"
                onChange={props.handelChange}
                value={props.formData.name}
            />
            <input
                type="text"
                name="description"
                className="add--form--input"
                placeholder="Product description"
                onChange={props.handelChange}
                value={props.formData.description}
            />
            <input
                type="number"
                min={0}
                name="price"
                className="add--form--input"
                placeholder="Product price"
                onChange={props.handelChange}
                value={props.formData.price}
            />
            <input
                type="number"
                min={1}
                name="quantity"
                className="add--form--input"
                placeholder="Product quantity"
                onChange={props.handelChange}
                value={props.formData.quantity}
            />
            <button className="add--form--button" onClick={props.addProduct}>
                <img src={plusLogo} alt="add" />
                <span>Add Product</span>
            </button>
        </form>
    );
}

AddForm.propTypes = {
    addProduct: PropTypes.func.isRequired,
    handelChange: PropTypes.func.isRequired,
    formData: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        quantity: PropTypes.number.isRequired,
        price: PropTypes.number.isRequired,
    }).isRequired,
};
