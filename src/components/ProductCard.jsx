import deleteLogo from "../images/Trash Can.svg";
import PropTypes from "prop-types";

export default function ProductCard(props) {
    return (
        <div className="product--card">
            <img
                src="https://images.pexels.com/photos/7879910/pexels-photo-7879910.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
                className="product--card--image"
                alt="grocery"
            />
            <div className="product--card--details">
                <h3 className="product--card--title" name={props.product.name}>{props.product.name}</h3>
                <p className="product--card--description">
                    {props.product.description}
                </p>
                <p className="product--card--price">
                    <b>$ {props.product.price /*.toFixed(2)*/}</b>
                </p>
                <p className="product--card--quantity">
                    Quantity: {props.product.quantity}
                </p>
                <button
                    className="product--card--button"
                    id={props.product.id}
                    onClick={props.handleClick}
                >
                    <img src={deleteLogo} alt="delete" />
                    <span>Delete</span>
                </button>
            </div>
        </div>
    );
}

ProductCard.propTypes = {
    product: PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        quantity: PropTypes.number.isRequired,
    }).isRequired,
    handleClick: PropTypes.func.isRequired,
};
