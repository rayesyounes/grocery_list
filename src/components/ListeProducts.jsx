import ProductCard from "./ProductCard";
import PropTypes from "prop-types";
import { useState } from "react";

export default function ListeProducts(props) {
    const sortedProducts = [...props.products];
    const [sortOption, setSortOption] = useState("none");

    function handleSortChange(event) {
        setSortOption(event.target.value);
    }

    if (sortOption === "high--price") {
        sortedProducts.sort((a, b) => b.price - a.price);
    } else if (sortOption === "low--price") {
        sortedProducts.sort((a, b) => a.price - b.price);
    }

    return (
        <>
            <div className="products--sort"></div>
            <div className="products--list">
                <select className="select" onChange={handleSortChange} value={sortOption}>
                    <option value="none" disabled hidden>Sort by...</option>
                    <option value="high--price">High Price</option>
                    <option value="low--price">Low Price</option>
                </select>
                {sortedProducts.map((product) => (
                    <ProductCard
                        key={product.id}
                        handleClick={() => props.deleteProduct(product.id)}
                        product={product}
                    />
                ))}
            </div>
        </>
    );
}

ListeProducts.propTypes = {
    products: PropTypes.array.isRequired,
    deleteProduct: PropTypes.func.isRequired,
};